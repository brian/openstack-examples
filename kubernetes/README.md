# Self-install Kubernetes on OpenStack

*Tip: Values in `$ALL_CAPS` should be customized before running the command.*

## Procedure

1. Deploy some servers, e.g. with the following openstack commands
    ```sh
    openstack server create k8s-controller --image "Ubuntu 18.04 LTS" --flavor mpcdf.medium       --network cloud-dmz-fixed --key-name $KEYNAME --user-data k8s-setup-ubuntu.sh
    openstack server create k8s-worker     --image "Ubuntu 18.04 LTS" --flavor mpcdf.large.bigmem --network cloud-dmz-fixed --key-name $KEYNAME --user-data k8s-setup-ubuntu.sh --min $NUM_WORKERS --max $NUM_WORKERS
    ```
    Reboot each servers once the setup script has finished, see `/var/log/cloud-init-output.log`.

2. Create the cluster
    ```sh
    # on the controller:
      kubeadm init --config k8s-init.yml
      echo "export KUBECONFIG=/etc/kubernetes/admin.conf" >> .bashrc && source .bashrc
      kubectl apply -f https://docs.projectcalico.org/v3.14/manifests/calico.yaml

    # on the workers:
      kubeadm join $CONTROLLER_IPADDR:6443 --token $JOIN_TOKEN --discovery-token-ca-cert-hash $JOIN_HASH
      ```

3. Enable [Cinder](https://github.com/kubernetes/cloud-provider-openstack/blob/master/docs/using-cinder-csi-plugin.md)-based dynamic persistent volumes (optional)
    ```sh
    # on the controller:
      git clone https://github.com/kubernetes/cloud-provider-openstack.git
      rm cloud-provider-openstack/manifests/cinder-csi-plugin/csi-secret-cinderplugin.yaml

      kubectl create secret -n kube-system generic cloud-config --from-file=cloud.conf  # cloud.conf provided by MPCDF upon request
      kubectl apply -f cloud-provider-openstack/manifests/cinder-csi-plugin
      kubectl create -f k8s-cinder.yml
    ```

## Example usage

* Publicly-accessible service
    ```sh
    # on the controller:
      kubectl create deployment echo --image=jmalloc/echo-server
      kubectl scale --replicas=5 deployment/echo
      kubectl expose deployment echo --port=80 --target-port=8080 --external-ip=$NODE_IPADDR

    openstack server add security group $NODE web
    curl http://$NODE_IPADDR
    ```
    *Note: Any (single) node can be used, determined by the given external IP.  The "web" security group should be added to (only) the corresponding node.*

* Pod with persistent storage
    ```sh
    # on the controller:
      kubectl apply -f k8s-pvc-demo.yml
      kubectl exec pvc-demo -- /bin/sh -c "echo Hallo > /data/file.txt"
      kubectl delete pod pvc-demo

      kubectl apply -f k8s-pvc-demo.yml
      kubectl exec pvc-demo -- cat /data/file.txt
    ```
